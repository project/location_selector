# Location Selector

- Do you want to assign the content of your website to one or more locations?
  Then this module is  perfect for you.


## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers



## Introduction

- With this module, you are able to assign worldwide locations to your content,
  without taxonomy and manual imports.

- [To submit bug reports and feature suggestions, or to track changes:]
  (https://www.drupal.org/project/issues/location_selector).


## Requirements

- Only a free GeoNames user account is needed (for free).
- See the "Configuration" section for more information.



## Installation

- [Install as you would normally install a contributed Drupal module. Visit:]
  (https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
  for further information.)



## Configuration

- [Go to](www.geonames.org/login) and create a free account for limitless API
   access.
- [See also](www.geonames.org/export/web-services) for more information.

- [Go to] (/admin/config/location_selector/settings) and enter the
   geonames username.

- On "Manage Fields": Add a location selector field to your entity.
   (e.g. Content Type)

- On "Manage Form Display": Set the Field Widget parameters.

- On "Manage Display": Set the Field Display parameters.

- If you want, create a view with this field as an exposed filter.

- [See more details/features on:]
  (https://www.drupal.org/project/location_selector)



## Troubleshooting

- Why does the Select list not appear?.

- Set your GeoNames Username at /admin/config/location_selector/settings.

- Why does the Select list still not appear?.

- Delete/Refresh/Rebuild the Drupal and browser cache.


## Maintainers

- Chris Casutt - [handkerchief](https://www.drupal.org/u/handkerchief)
- Shashank Kumar - [shashank5563](https://www.drupal.org/u/shashank5563)
- Jitendra verma - [jitendra-verma](https://www.drupal.org/u/jitendra-verma)

## This project has been sponsored by:
- Chris Casutt Realization
  Own realization agency, www.chriscasutt.ch
